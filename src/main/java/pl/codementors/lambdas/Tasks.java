package pl.codementors.lambdas;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;

/**
 * Some collection manipulation tasks.
 *
 * @author psysiu
 */
public class Tasks {

    public static void main(String[] args) {

        List<String> list = Arrays.asList("wolf", "tiger", "turtle", "tortoise", "puma", "wolverine", "");

        System.out.println("task1:");
        task1(list);
        System.out.println("\ntask2:");
        task2(list);
        System.out.println("\ntask3:");
        task3(list);
        System.out.println("\ntask4:");
        task4(list);
        System.out.println("\ntask5:");
        task5(list);
        System.out.println("\ntask6:");
        task6(list);
        System.out.println("\ntask7:");
        task7(list);
        System.out.println("\ntask8:");
        task8(list);
        System.out.println("\ntask9:");
        task9(list);
        System.out.println("\ntask10:");
        task10(list);
        System.out.println("\ntask11:");
        task11(list);
        System.out.println("\ntask12:");
        task12(list);

    }

    public static void task1(List<String> list) {
        list.forEach(String -> {
            System.out.println(String);
        });
        System.out.println();
       // list.forEach(System.out::println);
    }


    public static void task2(List<String> list) {
        list.stream().map(String::toUpperCase).forEach(System.out::println);
        System.out.println();
    }

    public static void task3(List<String> list) {
        //print only elements with 4 letters
        list.stream().filter(new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.length() == 4;
            }
        }).forEach(System.out::println);
    }
    public static void task4(List<String> list) {
        //print all elements sorted by number of letters
        list.stream().sorted(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        }).forEach(System.out::println);

        //list.stream().sorted(Comparator.comparingInt(String::length)).forEach(System.out::println);
    }

    public static void task5(List<String> list) {
        //print all elements starting with 't'
        list.stream().filter(s -> {
            if (s.length()>0) return s.charAt(0)=='t';
            return false;
        }).forEach(i -> System.out.println(i));

        list.stream().filter(s->s.startsWith("t")).forEach(System.out::println);
    }

    public static void task6(List<String> list) {
        //print number of all elements starting with w

        long count = list.stream().filter(s->s.startsWith("w")).count();
        System.out.println(count);

        System.out.println(list.stream().filter(s->s.startsWith("w")).count());
    }

    public static void task7(List<String> list) {
        //print "true" if any of the elements is empty, "false" otherwise
        boolean match;
//        boolean match = list.stream().anyMatch(new Predicate<String>() {
//            @Override
//            public boolean test(String s) {
//                return false;
//            }
//        });

        match = list.stream().anyMatch(String::isEmpty);
        System.out.println(match);
    }

    public static void task8(List<String> list) {
        //print "true" if all elements are empty, "false otherwise
        System.out.println(list.stream().allMatch(String::isEmpty));
    }

    public static void task9(List<String> list) {
        //print number of elements with more than 4 letters
    long count = list.stream().filter(s->s.length()>4).count();
        System.out.println(count);
    }

    public static void task10(List<String> list) {
        //print highest number of letters

        int max = list.stream().mapToInt(String::length).max().orElse(0);
        System.out.println(max);
    }

    public static void task11(List<String> list) {
        //print lowest number of letters

        int min = list.stream().mapToInt(String::length).min().orElse(0);
        System.out.println(min);
    }

    public static void task12(List<String> list) {
        //print sum of all numbers of letters
        int sum =  list.stream().mapToInt(String::length).sum();
        System.out.println(sum);
    }
}
