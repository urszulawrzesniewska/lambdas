package pl.codementors.lambdas.model;

/**
 * Interface for defining different ways of formatting texts.
 */
public interface Formatter {

    /**
     *
     * @param text Text to be formatted.
     * @return Formatted text.
     */
    String format(String text);

}
